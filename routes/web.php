<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/view_pembayaran', function () {
//     return view('pembayaran');
// });
Route::get('/','LoginController@login');

Route::get('view_pembayaran','FrontController@view_pembayaran');


Route::get('home','FrontController@index');

Route::post('komentar','FrontController@komentar');


Route::get('detail_shop/{id}','FrontController@detail_shop');

Route::get('register','LoginController@register');

Route::post('register_member','LoginController@register_member');

Route::get('login','LoginController@login');

Route::post('login_member','LoginController@login_member');

Route::get('logout','LoginController@logout');

Route::get('hapus_item/{id}','FrontController@hapus_item');

Route::get('about','FrontController@about');

Route::get('keranjang','FrontController@keranjang');

Route::post('cart','FrontController@cart');

Route::get('profile','FrontController@profile');

Route::post('update_keranjang','FrontController@update_keranjang');

Route::post('beli_sekarang','FrontController@beli_sekarang');

Route::get('kabupaten/{id}','FrontController@kabupaten');

Route::get('kecamatan/{id}','FrontController@kecamatan');

//awal order
Route::get('create_order','FrontController@create_order');

//lanjut order
Route::post('order','FrontController@order');

Route::post('update_order','FrontController@update_order');

Route::post('lunas','FrontController@lunas');

//metode bayar

Route::get('metode_bayar','FrontController@metode_bayar');
