@extends('template.tema')
@section('detail')

<div class="container">
	<div class="row">

		<div class="col-lg-3">
			<h1 class="my-4">Kategori Koi</h1>
			<div class="list-group">
				<h5><a href="{{url('home')}}" style="color: #4f9e19;text-decoration: none" class="list-group-item">Show All</a></h5>
				@foreach($kategori as $row)
				<h5><a href="{{url('home?id_jenis_ikan='.$row->id)}}" style="color: #4f9e19;text-decoration: none" class="list-group-item">{{$row->nama}}</a></h5>
				@endforeach
			</div>
		</div>
		<!-- /.col-lg-3 -->
		<div class="col-lg-9">
			<div class="card mt-4">
				<a href="{{asset($data->image)}}" data-lightbox="roadtrip"><img class="card-img-top img-fluid" src="{{asset($data->image)}}" alt=""></a>
				<div class="card-body">
					<h3 class="card-title">{{$data->nama}}</h3>
					<h4>Rp.{{number_format($data->harga)}}</h4>
					<p class="card-text"><?php echo $data->catatan ?></p>
					<span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
					4.0 stars
					<hr>
					<button data-toggle="modal" data-target="#beli" data-id="{{$_GET['id']}}" class="btn btn-sm btn-success hvr-float-shadow"><i class="fa fa-hand-o-right"></i>&nbsp;Beli Sekarang</button>
					<button class="btn btn-sm btn-danger hvr-float-shadow" onclick="cart()"><i class="fa fa-shopping-cart"></i>&nbsp;Masukan Ke Keranjang</button>
				</div>
			</div>
			<!-- /.card -->
			<div class="card mt-4">
				<div class="card-body">
					<form action="{{url('komentar')}}" method="post">
						@csrf
						<input type="hidden" name="id_data_ikan" value="{{Session::get('id_item')}}">
						<input type="hidden" name="id_user" value="{{Session::get('id')}}">
						<div class="form-group">
							<label for=""><h4>Review</h4></label>
							<textarea type='wysiwyg' class="form-control" name="komentar" id="summernote" required=""></textarea>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-sm btn-primary pointer hvr-float-shadow" value="Send Comment" name="">
						</div>
					</form>
				</div>
			</div>

			<div class="card card-outline-secondary my-4">
				<div class="card-header" style="background-color: #4f9e19">
					<h4 style="color: white">Product Reviews</h4>
				</div>
				<div class="card-body">
					@if($komentar)
					@foreach($komentar as $row)
					<?php $user=DB::table('users')->where('id',$row->id_user)->first(); if($user->nama){$posted=$user->nama;}else{$posted="Anonymous";} ?>
					<p><?php echo $row->komentar ?></p>
					<small class="text-muted">Posted by {{$posted}} on &nbsp; {{$row->created_at}}</small>
					<hr>
					@endforeach
					<!-- <a href="#" class="btn btn-success">Leave a Review</a> -->
					{{$komentar->links()}}
					@else
					@endif
				</div>
			</div>
			<!-- /.card -->

		</div>
		<!-- /.col-lg-9 -->

	</div>

</div>

<div>
	<form action="{{url('cart')}}" method="post" id="cart">
		@csrf
		<input type="hidden" name="status" value="waiting">
		<input type="hidden" name="id_user" value="{{Session::get('id')}}">
		<input type="hidden" name="jumlah_order" value="1">
		<input type="hidden" name="id_data_ikan" value="{{Session::get('id_item')}}">
	</form>
</div>

<div class="modal fade" id="beli" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="pull-left">Detail Belanja</h4>

				<button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<form method="post" action="{{url('beli_sekarang')}}" id="beli_sekarang">
					@csrf
				<div class="row">
					<div class="col-sm-6">
						<img src="{{asset($data->image)}}" width="200px" height="130px">
					</div>
					<div class="col-sm-6">
						<h3>{{$data->nama}}</h3>
						<p>Rp.{{number_format($data->harga)}}&nbsp;/ ekor</p>
						<div>
							<span class="btn btn-success btn-sm" onclick="kurang()"><i class="fa fa-minus"></i></span>
							<input type="number" class="form-control" style="width: 60px;display: -webkit-inline-box;height: 30px;" readonly="" min="1" id="jumlah_order" value="1" name="jumlah_order">
							<input type="hidden" name="id_data_ikan" value="{{Session::get('id_item')}}">
							<input type="hidden" name="harga" value="{{$data->harga}}">
							<span class="btn btn-success btn-sm" onclick="tambah()"><i class="fa fa-plus"></i></span>
						</div>
					</div>
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-sm btn-primary" onclick="beli_sekarang()">Beli</button>
			</div>
		</div>

	</div>
</div>


@endsection

@push('script')
<script>
	function beli_sekarang(){
		document.getElementById('beli_sekarang').submit();
			}
	function cart(){
		document.getElementById('cart').submit();
	}
	$('textarea').summernote({
		tabsize: 2,
		height: 200
	});

	 <?php if(Session::get('cart')){ ?>
	 	bootbox.confirm({
	 		message: "{{Session::get('cart')}}",
	 		buttons: {
	 			confirm: {
	 				label: 'Oke',
	 				className: 'btn-success'
	 			},
	 			cancel: {
	 				label: 'cancel',
	 				className: 'btn-danger'
	 			}
	 		},
	 		callback: function (result) {
	 			console.log('This was logged in the callback: ' + result);
	 		}
	 	});
    <?php } ?>

    function kurang(){
    	var jml=parseInt($('#jumlah_order').val())
    	if(jml!=0){
    		$('#jumlah_order').val(jml-1);
    		$('#jumlah_order').html(jml-1);
    	}else{
    		alert("jumlah order tidak boleh 0");
    	}
    }

    function tambah(){
    	var jml=parseInt($('#jumlah_order').val())
    	$('#jumlah_order').val(jml+1);
    	$('#jumlah_order').html(jml+1);
    }

</script>
@endpush