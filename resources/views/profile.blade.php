@extends('template.tema')
@section('profile')
<div class="card-profile" style="margin-top: 60px!important">
    <p class="login-box-msg" style="font-size: 25px">Edit Profile</p>

    <form action="{{url('edit_profile')}}" method="post" id="profile">
        @csrf
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="nama" value="{{$users->nama}}" placeholder="Full name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="email" class="form-control" value="{{$users->email}}" name="email" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password lama" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password baru" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
    </div>
     <div class="form-group has-feedback">
        <input type="number" class="form-control" name="hp" value="{{$users->hp}}" placeholder="phone">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
    </div>
     <div class="form-group">
        <input type="file" class="form-control" name="foto" placeholder="foto">
    </div>
</form>

<div class="social-auth-links text-center">
  <a href="#" class="btn btn-block btn-primary btn-flat hvr-float-shadow" onclick="profile()">Register</a>
</div>
</div>
@endsection

@push('script')

<script>
    function profile(){
        document.getElementById('profile').submit();
    }
</script>

@endpush