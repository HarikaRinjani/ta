@extends('template.tema_no_include')
@push('css')
<link rel="stylesheet" href="{{url('vendor/crudbooster/assets/adminlte/dist/css/AdminLTE.min.css')}}">
@endpush
@section('register')
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Koi</b>&nbsp;Farm</a>
</div>

@if (Session::get('message')!='')
<div class='alert alert-{{ Session::get("message_type") }}' style="text-align: left;">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-info"></i> {{ trans("crudbooster.alert_".Session::get("message_type")) }}</h4>
  {!!Session::get('message')!!}
</div>
@endif

<div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="{{url('register_member')}}" method="post" id="register">
        @csrf
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="nama" placeholder="Full name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" class="form-control" name="retype" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
    </div>
     <div class="form-group has-feedback">
        <input type="number" class="form-control" name="hp" placeholder="phone">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
    </div>
       <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" value="1" name="id_terms" style="margin-left: 15px"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
      </div>
</form>

<div class="social-auth-links text-center">
  <a href="#" class="btn btn-block btn-primary btn-flat hvr-float-shadow" onclick="register()">Register</a>
  <p>- OR -</p>
  <a href="{{url('login')}}" class="btn btn-block btn-danger btn-flat hvr-float-shadow">I already have a membership</a>
</div>
</div>
</div>

@endsection

@push('script')

<script>
    function register(){
        document.getElementById('register').submit();
    }
</script>

@endpush