@extends('template.tema')
@section('home')
  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <div class="col-lg-3">

        <h1 class="my-4">Kategori Koi</h1>
        <h5><a href="{{url('home')}}" style="color: #4f9e19;text-decoration: none;" class="list-group-item">Show All</a></h5>
        <div class="list-group">
        @foreach($kategori as $row)
          <h5><a href="{{'?id_jenis_ikan='.$row->id}}" style="color: #4f9e19;text-decoration: none" class="list-group-item">{{$row->nama}}</a></h5>
         @endforeach
        </div>
      </div>
      <!-- /.col-lg-3 -->

      <div class="col-lg-9">
        

        <div class="card-banner">

        <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
          <ol class="carousel-indicators">
          <?php $cek=count($banner); for ($i=0; $i <$cek; $i++){
           ?>
            <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="<?php if($i==1){echo "active";} ?>"></li>
        <?php }  $n=0; ?>
          </ol>
          <div class="carousel-inner" role="listbox">

          	@foreach($banner as $key)
            <div class="carousel-item <?php $n++; if($n==1){echo "active";} ?>">
              <a href="{{asset($key->image)}}" data-lightbox="roadtrip"><img width="900px" height="400px" class="d-block img-fluid" src="{{asset($key->image)}}" alt="First slide"></a>
            </div>
            @endforeach
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>

        <div class="row">

        @foreach($koi as $val)
        <?php $data=DB::table('image_koi')->where('id_data_ikan',$val->id)->first(); ?>

        <a href="{{url('detail_shop?id='.$val->id)}}" class="pointer">
        	<div class="col-lg-4 col-md-6 mb-4">
        		<div class="card h-100" style="border-radius: 15px">
        			<a href="{{asset($data->image)}}" data-lightbox="roadtrip" data-title="{{$val->nama}}"><img class="card-img-top" src="{{asset($data->image)}}" width="700px" height="180px" alt=""></a>
        			<div class="card-body">
        				<h4 class="card-title">
        					<a href="{{url('detail_shop/'.$val->id)}}">{{$val->nama}}</a>
        				</h4>
        				<h5>Rp.{{number_format($val->harga)}}</h5>
        				<p class="card-text"><?php echo $val->catatan; ?></p>
        			</div>
        			<div class="card-footer" style="background-color:#4f9e19">
        				<small class="text-white">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
        			</div>
        		</div>
        	</div>
        </a>

          @endforeach         
        </div>

        {{$koi->links()}}
        <!-- /.row -->

      </div>
      <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

@endsection