@extends('template.tema')

@section('about')

<div class="container" style="min-height: 450px">

	<div class="row">
		<div class="col-sm-12">
			<?php $i=1; ?>
			@foreach($data as $row)
			<?php $i++;  if($i==1){$mar="6rem";}else{$mar="1rem";} ?>
			<div class="card-rule" style="margin-top:{{$mar}}">
					{{$row->nama}}
					<hr>
					<p><?php echo $row->note; ?></p>
			</div>
			@endforeach
		</div>
	</div>
</div>

@endsection