@extends('template.tema_no_include')
@push('css')
<link rel="stylesheet" href="{{url('vendor/crudbooster/assets/adminlte/dist/css/AdminLTE.min.css')}}">
@endpush
@section('register')


<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Koi</b>&nbsp;Farm</a>
</div>

@if (Session::get('message')!='')
<div class='alert alert-{{ Session::get("message_type") }}' style="text-align: left;">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-info"></i> {{ trans("crudbooster.alert_".Session::get("message_type")) }}</h4>
  {!!Session::get('message')!!}
</div>
@endif

<div class="register-box-body">
  <p class="login-box-msg">Login a membership</p>

    <form action="{{url('login_member')}}" id="sub" method="post">
      @csrf
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="password">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="id_terms" value="1" style="margin-left: 15px"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
      </div>
</form>
<div class="social-auth-links text-center">
  <a href="javascript:void(0)" onclick="sub()" class="btn btn-block btn-primary btn-flat hvr-float-shadow">Login</a>
  <a href="{{url('register')}}" class="btn btn-block btn-danger btn-flat hvr-float-shadow">Register</a>
</div>
</div>
</div>

@endsection
@push('script')
<script>
  function sub(){
    document.getElementById('sub').submit();
  }
</script>
@endpush