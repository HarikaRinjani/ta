@extends('template.tema')
@section('cart')

<div class="container-fluid" style="min-height: 450px;margin-top: 70px">

	@if (Session::get('message')!='')
	<div class='alert alert-{{ Session::get("message_type") }}' style="text-align: left;margin-top: 15px">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-info"></i> {{ trans("crudbooster.alert_".Session::get("message_type")) }}</h4>
		{!!Session::get('message')!!}
	</div>
	@endif

	<div class="row">
		<div class="col-sm-8" style="margin-bottom: 3rem;">
			<div class="card-rule">
				<h2 style="margin-top: 20px"> Detail Belanja</h2>
				<table class="table table-hover" style="margin-top: 3rem">
					<thead>
						<tr>
							<th scope="col" class="pd-15">No</th>
							<th scope="col" class="pd-15">Image</th>
							<th scope="col" class="pd-15">Product</th>
							<th scope="col" class="pd-15">Price</th>
							<th scope="col" class="pd-15">Quantity</th>
							<th scope="col" class="pd-15">Status</th>
							<th scope="col" class="pd-15">Remove</th>
						</tr>
					</thead>
					<form id="update_keranjang" action="{{url('update_keranjang')}}" method="post">
						<tbody>
							<?php $i=1;?>
							@foreach($data as $key)
							<?php $img=DB::table('image_koi')->where('id_data_ikan',$key->id_data_ikan)->first(); ?>
							<tr style="cursor: pointer;">
								<th scope="row" class="pd-15">{{$i++}}</th>
								<td class="pd-15"><a href="{{$img->image}}" data-lightbox="roadtrip"><img src="{{$img->image}}"width="100px" height="auto"></a></td>
								<td class="pd-15"  onclick="mov({{$key->id}})">{{$key->nama}}</td>
								<td class="pd-15">Rp.{{number_format($key->harga)}}</td>
								<td class="pd-15">
									@csrf
									<div>
										<span class="btn btn-success btn-sm" id="min-jumlah" onclick="min({{$i}})"><i class="fa fa-minus"></i></span>
										<input type="number" class="form-control" style="width: 60px;display: -webkit-inline-box;height: 30px;" readonly="" min="1" id="jumlah_order{{$i}}" value="{{$key->jumlah_order}}" name="jumlah_order[]">
										<input type="hidden" name="status[]" value="{{$key->status_shop}}">
										<input type="hidden" name="id_order[]" value="{{$key->id_shop}}">
										<input type="hidden" name="id_data_ikan[]" value="{{$key->id_data_ikan}}">
										<span class="btn btn-success btn-sm" onclick="plus({{$i}})" id="plus-jumlah{{$i}}"><i class="fa fa-plus"></i></span>
									</div>
								</td>
								<?php
								if($key->status_shop=="waiting"){
									$label="primary";
								}elseif($key->status_shop=="proses"){
									$label="success";
								}else{
									$label="danger";
								}

								?>
								<td class="pd-15"><button class="btn btn-sm btn-{{$label}}" style="text-transform: capitalize;">{{$key->status_shop}}</button></td>
								<td class="pd-15"><a href="javascript:void(0)" class="btn btn-sm btn-warning" 
									onclick="swal({
										title: 'Apakah Anda Ingin Menghapusnya ?',
										type:'info',
										showCancelButton:true,
										allowOutsideClick:true,
										confirmButtonColor: '#DD6B55',
										confirmButtonText: 'Yes',
										cancelButtonText: 'No',
										closeOnConfirm: false
									}, function(){
										location.href ='hapus_item/'+'{{$key->id_shop}}';
									});"><i class="fa fa-trash text-white"></i></a></td>
								</tr>
								@endforeach
							</tbody>
						</form>
					</table>
	<!-- </div>

		<div class="col-sm-6"> -->
			<a href="{{url('home')}}" class="btn btn-xl btn-primary hvr-float-shadow">Lanjut Belanja</a>
			<button type="button" onclick="update_keranjang()" class="btn btn-outline-primary hvr-float-shadow">Update Keranjang Belanja</button>
		</div>
	</div>

	<div class="col-sm-4">
		<div class="card-rule">
			<h3>Tota Belanja</h3>
			<table class="table table-borderless">
				<th>Nama</th>
				<th>Quantity</th>
				<th>Harga</th>
				<tbody>
					@foreach($data as $row)
					<tr>
						<td>{{$row->nama}}</td>
						<td>{{$row->jumlah_order}}</td>
						<td>Rp.{{number_format($row->harga*$row->jumlah_order)}}</td>
						<?php $jumlah+=$row->harga*$row->jumlah_order; $order+=$row->jumlah_order; ?>
					</tr>
					@endforeach
					<tr>
						<th>Total</th>
						<th>{{$order}}</th>
						<th>Rp.{{number_format($jumlah)}}</th>
					</tr>
				</tbody>
			</table>
			<?php  $cek=count($data); if($cek!=0){
				$dis="";
			}else{
				$dis="disabled";
			} ?>
			<a href="{{url('create_order')}}" <?php echo $dis; ?> class="btn btn-outline-primary hvr-float-shadow">Process And Checkout</a>
		</div>

	</div>
</div>




</div>

@endsection


@push('script')
<script>
	function hapus(id){
		swal({
			title: 'Apakah Anda Ingin Menghapusnya ?',
			type:'info',
			showCancelButton:true,
			allowOutsideClick:true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes',
			cancelButtonText: 'No',
			closeOnConfirm: false
		}, function(){
			location.href = 'hapus_item/'+id;
		});
	};

	function mov(id){
		location.href = "{{url('detail_shop?id=')}}"+id;
	};

	function update_keranjang(){
		document.getElementById('update_keranjang').submit();
	}

	function min(i){
		var jml=parseInt($('#jumlah_order'+i).val())
		if(jml!=0){
			$('#jumlah_order'+i).val(jml-1);
			$('#jumlah_order'+i).html(jml-1);
		}else{
			alert("jumlah order tidak boleh 0");
		}
	}

	function plus(i){
		var jml=parseInt($('#jumlah_order'+i).val())
		$('#jumlah_order'+i).val(jml+1);
		$('#jumlah_order'+i).html(jml+1);
	}

	function bayar(){
		document.getElementById('order').submit();
	}

</script>

@endpush