@extends('template.tema')
@section('pembayaran')

<div class="container-fluid" style="min-height: 500px;margin-top: 70px">

  @if (Session::get('message')!='')
  <div class='alert alert-{{ Session::get("message_type") }}' style="text-align: left;margin-top: 15px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> {{ trans("crudbooster.alert_".Session::get("message_type")) }}</h4>
    {!!Session::get('message')!!}
  </div>
  @endif

  <div class="row">
    <!--   <form method="post" action="" id="lunas"> -->
     <div class="col-sm-7">
      <div class="card-belanja">
       <h4>Detail Pembeli</h4>
       <hr>
       <div class="border-belanja">
        <form method="post" action="{{url('lunas')}}" id="data-diri">
          {{csrf_field()}}
          <input type="hidden" name="id" value="{{$_GET['id_order']}}">
          <input type="hidden" name="id_ongkir" id="ongkos_kirim">
          <input type="hidden" name="total_harga" id="total_price">
          <input type="hidden" name="catatan" id="catatan">
          <div class="row">
            <div class="col-sm-12">

              <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" name="nama" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
              </div>

              <div class="row">
               <div class="col-sm-6">

                 <div class="form-group">
                   <label for="exampleInputEmail1">Email Pembeli</label>

                   <input type="email" name="email" class="form-control" placeholder="Email">
                 </div>
               </div>
               <div class="col-sm-6">
                 <div class="form-group">
                   <label for="exampleInputEmail1">Hp Pembeli</label>

                   <input type="number" name="hp" class="form-control" placeholder="Hp">
                 </div>
               </div>
             </div>

             <div class="form-group">

              <button type="button" data-toggle="modal" data-target="#modal-default" class="btn btn-outline-primary hvr-float-shadow">Masukan Alamat Pengirim</button>

            </div>
          </div>
        </div>
        @foreach($data as $input)
        <input type="hidden" value="{{$input->id_shop}}" name="id_shopping[]">
        @endforeach
      </form>
    </div>


    <br>

    <div class="border-belanja">
      <div class="row">
        <div class="col-sm-12">

          <div class="form-group">
            <label>Pilih Kurir</label>
            <select class="form-control select2" name="id_ongkir" id="ongkir" style="width: 100%;">
              <option>--Silahkan Pilih Ongkir--</option>
              @foreach($ongkir as $row)
              <option value="{{$row->harga}}" data-id="{{$row->id}}">
                <table border="0px">
                  <tr>
                    <td style="padding-right: 5px">{{$row->nama}}</td>
                    <td style="padding-right: 5px">Rp.{{number_format($row->harga)}}</td>
                  </tr>
                </table>
              </option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
           <label>Catatan</label>
           <textarea class="form-control" name="note" id="note"></textarea>
         </div>

       </div>

     </div>

   </div>
 </div>


</div>

<!-- </form> -->

<?php

foreach ($data as $key) {

  $total_harga+=$key->harga*$key->jumlah_order;

}

?>


<div class="col-sm-5">
  <div class="card-belanja">
   <h4>Ringkasan Belanja</h4>
   <hr>
   <table class="table table-borderless">
    <tr>
      <td>Total Harga Barang</td>
      <td><p id="total_harga" data-id="{{$total_harga}}">Rp.{{number_format($total_harga)}}</p></td>
    </tr>
    <tr>
      <td>Biaya Kirim</td>
      <td><p id="biaya-kirim">...</p></td>
    </tr>
  </table>

  <hr>
  <table class="table table-borderless">
    <tfoot>
      <tr>
        <td><b>Total Belanja</b></td>
        <td><p id="total_belanja"></p></td>
      </tr>
    </tfoot>
  </table>

  <p style="text-align: center;">
    <button style="width: 100%;" onclick="simpan()" class="btn hvr-float-shadow btn-primary">Pilih Metode Bayar</button>
  </p>

</div>
</div>

<div class="col-sm-7" style="margin-top: 30px">
  <div class="card-belanja">

   <h4>Detail Belanja</h4>
   <hr>
   <div class="border-belanja">
    <div class="row">
      <div class="col-sm-12">
       <table class="table table-borderless">
        <tbody>
          <th>Image</th>
          <th>Items</th>
          <th>Quantity</th>
          <th>Price</th>
        </tbody>
        @foreach($data as $key)
        <?php $img=DB::table('image_koi')->where('id_data_ikan',$key->id_data_ikan)->first(); ?>
        <tr>
          <td><a href="{{$img->image}}" data-lightbox="roadtrip"><img src="{{$img->image}}"width="100px" height="auto"></a></td>
          <td>{{$key->nama}}</td>
          <td>{{$key->jumlah_order}}</td>
          <td>Rp.{{number_format($key->harga*$key->jumlah_order)}}</td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
</div>
<br>

</div>
</div>


<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
   <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <h4 class="modal-title pull-left">Isikan Alamat Dengan Benar</h4>
    </div>
    <div class="modal-body">
      <form id="alamat">
        {{csrf_field()}}
        <input type="hidden" name="id" value="{{$_GET['id_order']}}">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label>Provinsi</label>
              <select class="form-control select2" id="prov" name="id_provinsi" style="width: 100%;">
                <option>--Silahkan Pilih Provinsi--</option>
                @foreach($provinsi as $key)
                <option value="{{$key->id}}">{{$key->nama}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>Kabuaten</label>
              <select class="form-control select2" id="kab" name="id_kabupaten" style="width: 100%;">
                <option id="Kabupaten" data-id="" value="">--Silahkan Pilih Kabupaten--</option>
              </select>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
             <label>Kecamatan</label>
             <select class="form-control select2" id="kec" name="id_kecamatan" style="width: 100%;">
              <option>--Silahkan Pilih Kecamatan--</option>>
            </select>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group">
           <label for="exampleInputEmail1">Alamat Lengkap</label>
           <textarea cols="63" class="form-control" name="alamat" id="alamat" rows="3"></textarea>
         </div>
       </div>
     </div>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <button type="submit" id="save"  class="btn btn-primary">Save changes</button>
  </div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection

@push('script')

<script>
  function simpan(){
    var catatan=document.getElementById('note').value;
    document.getElementById('catatan').value=catatan;
    document.getElementById('data-diri').submit();
  }
  $(document).ready(function(){
    $('#prov').on('change',function(e){            
     var id_provinsi = $('#prov').val();
     $.get("{{url('kabupaten')}}/"+id_provinsi,function(data){
      var json = JSON.stringify(data);
      var fromJson = JSON.parse(json);
      var length =  Object.keys(fromJson).length;
      var propCount = Object.keys(data).length;
      if(data){
        $.each(data,function(i,value){
          $('#kab').append($('<option>',{ 
            value :value.id,
            text : value.nama 
          }))
        })
      }
    })
     if(window.propCount==0){
       $("#kab").html("<option>-- Tidak Ada Kabupaten ---</option>");
     }else{
      $("#kab").html("<option>-- Silahkan Pilih Kabupaten ---</option>");
    }
  });

    $('#kab').on('change',function(e){            
     var id_kab = $('#kab').val();
     console.log(id_kab);
     $.get("{{url('kecamatan')}}/"+id_kab,function(data){
      var json = JSON.stringify(data);
      var fromJson = JSON.parse(json);
      var length =  Object.keys(fromJson).length;
      var propCount = Object.keys(data).length;
      if(data){
        $.each(data,function(i,value){
          $('#kec').append($('<option>',{ 
            value:value.id,
            text : value.nama 
          }));
        })
      }
    })
     if(window.propCount==0){
       $("#kec").html("<option>-- Tidak Ada Kabupaten ---</option>");
     }else{
      $("#kec").html("<option>-- Silahkan Pilih Kabupaten ---</option>");
    }
  });

    <?php $cek=count($ongkir);?>
    $('#ongkir').on('change',function(e){      
     var ongkos = $('#ongkir').val();

     var option = $('option:selected', this).attr('data-id');
//set input ongkos kirim
$('#ongkos_kirim').val(option);

console.log(ongkos);


var total_harga=$('#total_harga').attr('data-id');
var total_belanja=Number(ongkos)+Number(total_harga);

//set inpu harga total;

           //merubah angka ke rupiah
           var  tb = total_belanja.toString(),
           sisa  = tb.length % 3,
           rupiah  = tb.substr(0, sisa),
           ribuan  = tb.substr(sisa).match(/\d{3}/g);
           if (ribuan) {
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
          }
          var  th = ongkos.toString(),
          sisa1  = th.length % 3,
          rupiah1  = th.substr(0, sisa1),
          ribuan1  = th.substr(sisa1).match(/\d{3}/g);
          if (ribuan1) {
            separator1 = sisa1 ? ',' : '';
            rupiah1 += separator1 + ribuan1.join(',');
          }
          $('#biaya-kirim').text("Rp."+rupiah1);
          $('#total_belanja').text("Rp."+rupiah);
          $('#total_price').val(total_belanja);
        })

    //create_order

    $('#alamat_sample').on('submit',function(event){
      event.preventDefault();
      $.ajax({
        url:"{{url('order')}}",
        type:"POST",
        data:$('#alamat').serialize(),
        success:function(response)
        {
         console.log(response)
         $('#modal-default').modal('hide')
         alert('data saved');
       },
       error:function(error){
        console.log(error)
        alert('data not save');
      }
    })
    });

    //update_order

    $('#alamat').on('submit',function(event){
      event.preventDefault();
      $.ajax({
        url:"{{url('update_order')}}",
        type:"POST",
        data:$('#alamat').serialize(),
        success:function(response)
        {
         console.log(response)
         $('#modal-default').modal('hide')
         alert('data saved');
       },
       error:function(error){
        console.log(error)
        alert('data not save');
      }
    })
    });

  });
</script>

@endpush