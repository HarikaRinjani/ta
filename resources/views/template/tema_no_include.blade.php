<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>TA</title>
	@include('template.head')
	@stack('css')
</head>
<body>

	<!-- register -->
	@yield('register')

	@stack('script')

</body>

</html>
