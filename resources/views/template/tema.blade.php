<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>TA</title>
	@include('template.head')
</head>
<body>
	@include('template.header')
<div style="margin-top: 30px!important">
    <!-- home -->
	@yield('home')

	<!-- detial -->
	@yield('detail')

	<!-- about -->
	@yield('about')

	<!-- cart -->
	@yield('cart')

	<!-- profile -->
	@yield('profile')

	<!-- pembayaran -->
	@yield('pembayaran')

	<!-- metode bayar -->

	@yield('metode_bayar')

	@include('template.footer')
	@include('template.foot')

	<!-- register -->
	@yield('register')
</div>

	@stack('script')

<script>
  function logout(){
    swal({
      title: 'Do you want to logout',
      type:'info',
      showCancelButton:true,
      allowOutsideClick:true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      closeOnConfirm: false
    }, function(){
      location.href = 'logout';
    });
  };

  lightbox.option({
  	'resizeDuration': 400,
  	'wrapAround': true
  })

</script>
</body>

</html>
