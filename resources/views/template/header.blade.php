<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="height:95px; background-image: linear-gradient(#00c6ff,#0072ff);">
  <div class="container"><h4>
    <a class="navbar-brand" href="{{url('home')}}" style="font-size: 30px;cursor: pointer;">Koi Farm</a></h4>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
         <h5><a class="nav-link text-white" style="cursor: pointer;" href="{{url('home')}}">Home
          <span class="sr-only">(current)</span>
        </a></h5>
      </li>
      <li class="nav-item">
        <h5><a class="nav-link text-white" style="cursor: pointer;" href="{{url('about')}}">About</a></h5>
      </li>
      <li class="nav-item">
        <h5><a class="nav-link text-white" style="cursor: pointer;" href="#">Contact</a></h5>
      </li>
      <?php $user=DB::table('shopping')->where('id_user',Session::get('id'))->where('status','waiting')->count(); ?>
      @if(Session::get('id'))
      <li class="nav-item">
        @if($user)
        <h5><a class="nav-link text-white" style="cursor: pointer;" href="{{url('keranjang')}}"><i class="fa fa-shopping-cart"></i>&nbsp;<span style="border-radius: 40%;
        padding: 4px;
        background: orange;
        color: white;
        text-align: center;
        font: 12px Arial, sans-serif;">{{$user}}</span></a></h5>
        @else
        <h5><a class="nav-link text-white" style="cursor: pointer;" href="{{url('keranjang')}}"><i class="fa fa-shopping-cart"></i></a></h5>
        @endif
      </li>
      @endif
      <?php $mem=DB::table('users')->where('id',Session::get('id'))->first(); ?>
      @if(Session::get('id'))
      <li class="dropdown user user-menu" style="margin-top: 5px;margin-left: 5px;">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="{{url('vendor/crudbooster/avatar.jpg')}}" class="img-circle" style="border-radius: 50%" height="auto" width="40px" alt="User Image">
          <!-- <span class="hidden-xs text-white">{{$mem->nama}}</span> -->
        </a>
        <ul class="dropdown-menu pull-left" style="width: 350%;padding: 20px;">
          <!-- User image -->
          <li class="user-header">
            <img src="{{url('vendor/crudbooster/avatar.jpg')}}" width="80px" style="border-radius: 50%" class="center" alt="User Image">
            <p style="text-align: center;">
              {{$mem->nama}}<br>
              <small>{{date('Y-m-d H:i:s')}} </small>
            </p>
          </li>
          <!-- Menu Body -->
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <a href="{{url('profile')}}" class="btn btn-outline-danger">Profile</a>
            </div>
            <div class="pull-right">
              <a href="javascript:void(0)" onclick="logout()" class="btn btn-outline-primary">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
      @else
      <li class="nav-item">
       <h5><a class="nav-link" href="{{url('login')}}">Login</a></h5>
     </li>
     @endif
   </ul>
 </div>
</div>
</nav>
