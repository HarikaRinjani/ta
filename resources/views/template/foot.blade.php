  <script src="{{url('assets/jquery/jquery.min.js')}}"></script>
  <script src="{{url('assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{url('vendor/crudbooster/assets/adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{url('vendor/crudbooster/assets/summernote/summernote.min.js')}}"></script>
  <script src="{{url('vendor/crudbooster/assets/sweetalert/dist/sweetalert.min.js')}}"></script>
  <script src="{{url('assets/js/popper.min.js')}}"></script>
  <script src="{{url('assets/js/bootbox.min.js')}}"></script>
  <script src="{{url('assets/js/bootbox.locales.min.js')}}"></script>
  <script src="{{url('vendor/crudbooster/assets/select2/dist/js/select2.full.js')}}"></script>
  <script src="{{url('vendor/crudbooster/assets/lightbox/dist/js/lightbox.js')}}"></script>
  