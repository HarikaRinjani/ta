<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use Session;
use Response;

class FrontController extends Controller
{
    public function index(){
    	$banner=DB::table('banner')
    	->join('banner_role','banner.id_banner_role','=','banner_role.id')
    	->where('banner_role.nama','shop')
    	->get();

        $data_ikan=DB::table('data_ikan')->where('status','Dijual')->get();


        if($_GET['id_jenis_ikan']){

          $data_show=DB::table('data_ikan')->where('status','Dijual')->where('id_jenis_ikan',$_GET['id_jenis_ikan'])->paginate(3);

        }else{

          $data_show=DB::table('data_ikan')->where('status','Dijual')->paginate(3);


      }

      $cek=[];

      foreach ($data_ikan as $key) {
        $list=$key->id_jenis_ikan;
        array_push($cek, $list);
    }

    $kategori=DB::table('master_jenis_ikan')->whereIn('id',$cek)->get();


    $data['banner']=$banner;
    $data['kategori']=$kategori;
    $data['koi']=$data_show;
    return view('home',$data);
}

public function detail_shop($id){

  Session::put('id_item',$id);

   $data=DB::table('data_ikan')->join('image_koi','data_ikan.id','=','image_koi.id_data_ikan')
   ->where('data_ikan.id',$id)->where('data_ikan.status','Dijual')->first();

   $sidbar=DB::table('data_ikan')->where('status','Dijual')->get();

   $cek=[];

   foreach ($sidbar as $key) {
    $list=$key->id_jenis_ikan;
    array_push($cek, $list);
}

$kategori=DB::table('master_jenis_ikan')->whereIn('id',$cek)->get();     
$response['kategori']=$kategori;

$komentar=DB::table('komentar')
->where('id_data_ikan',$id)->paginate(5);
$response['data']=$data;
$response['komentar']=$komentar;
return view('detail_shop',$response);
}


public function komentar(Request $request){
  if($request->komentar){
    $data['created_at']=date('Y-m-d H:i:s');
    $data['komentar']=$request->komentar;
    $data['id_data_ikan']=$request->id_data_ikan;
    $data['id_user']=$request->id_user;

    $kw=DB::table('komentar')->insert($data);
    if($kw){
        return redirect()->back()->with(['cart'=>'Komentar Berhasil Di Publish']);

    }else{
      return redirect()->back()->with(['cart'=>'Komentar Gagal Di Publish']);
  }  

  }else{

      return redirect()->back()->with(['cart'=>'Tidak Ada Komentar']);

  } 

}

public function keranjang(){

    $shopping=DB::table('shopping')
    ->join('data_ikan','shopping.id_data_ikan','=','data_ikan.id')
    ->where('shopping.id_user',Session::get('id'))
    ->select('shopping.status as status_shop','shopping.id_data_ikan','shopping.id as id_shop','shopping.jumlah_order','data_ikan.*')->get();

    $data['data']=$shopping;

    return view('keranjang',$data);

}

public function cart(Request $request){
    if(Session::get('id')){
        $data['id_user']=$request->id_user;
        $data['id_data_ikan']=$request->id_data_ikan;
        $data['status']=$request->status;
        $data['jumlah_order']=$request->jumlah_order;
        $data['created_at']=date('Y-m-d H:i:s');
        $kw=DB::table('shopping')->insert($data);
        if($kw){
            return redirect()->back()->with(['cart'=>'Berhasil Memasukan Items Ke Keranjang']);

        }else{
          return redirect()->back()->with(['cart'=>'Gagal Memasukan Items Ke Keranjang']);
      }   

  }else{
   return redirect()->action('LoginController@login')->with(['message'=>'Anda Harus Login Terlebih Dahulu','message_type'=>'danger']);
}
}

public function hapus_item($id){
    DB::table('shopping')->where('id',$id)->delete();
    if($id){
        return redirect()->back()->with(['message'=>'Berhasil Menghapus','message_type'=>'primary']);

    }else{
      return redirect()->back()->with(['message'=>'Gagal Menghapus','message_type'=>'warning']);
  }  

}

public function about(){

    $about=DB::table('about')->get();

    $data['data']=$about;
    return view('about',$data);
}

public function profile(){
    $user=DB::table('users')->where('id',Session::get('id'))->first();
    $data['users']=$user;
    return view('profile',$data);
}

public function update_keranjang(){
$jumlah_order=g('jumlah_order');
$id_data_ikan=g('id_data_ikan');
$status=g('status');
$id=g('id_order');

$hapus=DB::table('shopping')->whereIn('id',$id)->delete();

if($hapus){
  $i=0;
  $order=[];
  foreach ($jumlah_order as $key) {
    $list['jumlah_order']=$jumlah_order[$i];
    $list['id_data_ikan']=$id_data_ikan[$i];
    $harga=DB::table('data_ikan')->where('id',$id_data_ikan[$i])->first();
    $list['id_user']=Session::get('id');
    $list['status']=$status[$i];
    $list['price']=$jumlah_order[$i]*$harga->harga;
    $i++;
    array_push($order, $list);
  }
  $cek=DB::table('shopping')->insert($order);
  if($cek){
    return redirect()->back()->with(['message'=>'Berhasil Mengupdate','message_type'=>'primary']);

  }else{
    return redirect()->back()->with(['message'=>'Gagal Mengupdate','message_type'=>'warning']);
  }  

}


}
public function beli_sekarang(){


if(Session::get('id')){

  $cek=DB::table('order')->where('id_user',Session::get('id'))->where('status','waiting')->first();
  if($cek==null){
    $order['status']="waiting";
    $order['id_user']=Session::get('id');
    $id_get=DB::table('order')->insertGetId($order);
  }else{
    $id_get=$cek->id;

  }
  $data['id_data_ikan']=g('id_data_ikan');
  $data['price']=g('harga')*g('jumlah_order');
  $data['jumlah_order']=g('jumlah_order');
  $data['id_user']=Session::get('id');
  $data['created_at']=date('Y-m-d H:i:s');
  $data['status']="waiting";

  $cek=DB::table('shopping')->insert($data);
    if($cek){
    return redirect()->action('FrontController@view_pembayaran',['id_order'=>$id_get])->with(['message'=>'Berhasil Menyimpan','message_type'=>'primary']);

  }else{
    return redirect()->back()->with(['message'=>'Gagal Menyimpan','message_type'=>'warning']);
  } 
}else{
 return redirect()->action('LoginController@login')->with(['message'=>'Anda Harus Login Terlebih Dahulu','message_type'=>'danger']);
}

}

public function pembayaran(){

}

public function view_pembayaran(){

    $shopping=DB::table('shopping')
    ->join('data_ikan','shopping.id_data_ikan','=','data_ikan.id')
    ->where('shopping.id_user',Session::get('id'))
    ->select('shopping.status as status_shop','shopping.id_data_ikan','shopping.id as id_shop','shopping.jumlah_order','data_ikan.*')->get();

    $provinsi=DB::table('provinsi')->get();

    $ongkir =DB::table('ongkir')->get();

    $order=DB::table('order')
    ->join('provinsi','order.id_provinsi','=','provinsi.id')
    ->join('kabupaten','order.id_kabupaten','=','kabupaten.id')
    ->join('kecamatan','order.id_kecamatan','=','kecamatan.id')
    ->where('id_user',Session::get('id_user'))->where('status','waiting')
    ->select('order.*','provinsi.nama as provinsi','kabupaten.nama as kabupaten','kecamatan.nama as kecamatan')
    ->first();

    $data['data']=$shopping;
    $data['order']=$order;
    $data['provinsi']=$provinsi;
    $data['ongkir']=$ongkir;


  return view('pembayaran',$data);
}

public function kabupaten($id){

  $kabupaten=DB::table('kabupaten')->where('id_provinsi',$id)->get();
  return response()->json($kabupaten);

}

public function kecamatan($id){

  $kecamatan=DB::table('kecamatan')->where('id_kabupaten',$id)->get();
  return response()->json($kecamatan);

}

//order semua ini klo lupa ingat lagi gaes

public function create_order(){
  if(Session::get('id')){
  $cek=DB::table('order')->where('id_user',Session::get('id'))->where('status','waiting')->first();
  if($cek==null){
    $order['status']="waiting";
    $order['id_user']=Session::get('id');
    $id_get=DB::table('order')->insertGetId($order);
  }else{
    $id_get=$cek->id;
  }
if($id_get){
  return redirect()->action('FrontController@view_pembayaran',['id_order'=>$id_get]);
}else{
  return redirect()->back()->with(['message'=>'Silahkan Pilih Ikan Yang Akan Di beli','message_type'=>'warning']);
}

}else{
   return redirect()->action('LoginController@login')->with(['message'=>'Anda Harus Login Terlebih Dahulu','message_type'=>'danger']);
}

}

public function order(Request $request,$id){
  $data['id_provinsi']=$request->input('id_provinsi');
  $data['id_kabupaten']=$request->input('id_kabupaten');
  $data['id_kecamatan']=$request->input('id_kecamatan');
  $data['alamat']=$request->input('alamat');
  $data['created_at']=date('Y-m-d H:i:s');
  $ok=DB::table('order')->insert($data);

  return response()->json($data);
}

public function update_order(Request $request){
  $data['id']=$request->input('id');
  $data['id_provinsi']=$request->input('id_provinsi');
  $data['id_kabupaten']=$request->input('id_kabupaten');
  $data['id_kecamatan']=$request->input('id_kecamatan');
  $data['alamat']=$request->input('alamat');
  $data['created_at']=date('Y-m-d H:i:s');
  $ok=DB::table('order')->where('id',$request->input('id'))->update($data);

  return response()->json($data);
}

public function lunas(Request $request){
  $data['id']=$request->id;
  $data['nama']=$request->nama;
  $data['email']=$request->email;
  $data['hp']=$request->hp;
  $data['id_ongkir']=$request->id_ongkir;
  $data['total_harga']=$request->total_harga;
  $data['catatan']=$request->catatan;
  $id_shop=$request->id_shopping;
  $ok=DB::table('order')->where('id',$request->id)->update($data);
  $id_order['id_order']=$request->id;
  DB::table('shopping')->whereIn('id',$id_shop)->update($id_order);
  if($ok){
    return view('metode_bayar');
  }else{
    return redirect()->back()->with(['message'=>'Data Belum Di lengkapi','message_type'=>'danger']);
  }
}



}
