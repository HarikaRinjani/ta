<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminDataIkanController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "nama";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "data_ikan";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Jenis Ikan","name"=>"id_jenis_ikan","join"=>"master_jenis_ikan,nama"];
			$this->col[] = ["label"=>"Nama","name"=>"nama"];
				$this->col[] = ["label"=>"Size","name"=>"size","callback"=>function($row){
				return $row->size." cm";
			}];
					$this->col[] = ["label"=>"Status","name"=>"status","callback"=>function($row){

			if ($row->status=='Approve') 
			{
				return '<div class="btn-group">
                  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
                    Approve <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                  	<li>
                  		<a onclick="approve('.$row->id.')" style="cursor:pointer">Approve</a>
                   	</li>
                   	<li>
                   	<a onclick="javascript:void(0)" data-id="'.$row->id.'" data-toggle="modal" data-target="#dijual" id="atc"  style="cursor:pointer">Dijual</a>
                   	</li>
                  </ul>
                </div>';
			}elseif($row->status=='Dijual'){
				return '<span class="label label-danger">'.$row->status.'</span>';
		
			}elseif($row->status=='Terjual'){
			   return '<span class="label label-info">'.$row->status.'</span>';
			}elseif($row->status=='waiting'){
				return '<div class="btn-group">
                  <button type="button" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown">
                    waiting <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    	<li>
                  		<a onclick="approve('.$row->id.')" style="cursor:pointer">Approve</a>
                   	</li>
                   	<li>
                   	<a onclick="javascript:void(0)" data-id="'.$row->id.'" data-toggle="modal" data-target="#dijual" id="atc"  style="cursor:pointer">Dijual</a>
                   	</li>
                  </ul>
                </div>';
			}
			}];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Jenis Ikan','name'=>'id_jenis_ikan','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'master_jenis_ikan,nama'];
			$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Size','name'=>'size','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Catatan','name'=>'catatan','type'=>'wysiwyg','width'=>'col-sm-10'];

			$columns[] = ['label' => 'Gambar', 'name' => 'image', 'type' => 'upload','validation' => 'required|image|max:1000'];
			$this->form[] = ['label' => 'Image Koi', 'name' => 'image_koi', 'type' => 'child', 'columns' => $columns, 'table' => 'image_koi', 'foreign_key' => 'id_data_ikan'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Jenis Ikan","name"=>"id_jenis_ikan","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"jenis_ikan,id"];
			//$this->form[] = ["label"=>"Nama","name"=>"nama","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
			//$this->form[] = ["label"=>"Size","name"=>"size","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Catatan","name"=>"catatan","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;

	        $this->script_js=" 
			  	function approve(id){
			        swal({
			            title: 'Do you want to approve ?',
			            type:'info',
			            showCancelButton:true,
			            allowOutsideClick:true,
			            confirmButtonColor: '#DD6B55',
			            confirmButtonText: 'Yes',
			            cancelButtonText: 'No',
			            closeOnConfirm: false
			        }, function(){
			            location.href = '".CRUDBooster::mainpath("approve/")."'+id;

			        });
			    };

			    function dijual(id){
			        swal({
			            title: 'Do you want to reject ?',
			            type:'info',
			            showCancelButton:true,
			            allowOutsideClick:true,
			            confirmButtonColor: '#DD6B55',
			            confirmButtonText: 'Yes',
			            cancelButtonText: 'No',
			            closeOnConfirm: false
			        }, function(){
			            location.href = '".CRUDBooster::mainpath("dijual/")."'+id;
			        });
			    };

			        function terjual(id){
			        swal({
			            title: 'Do you want to process ?',
			            type:'info',
			            showCancelButton:true,
			            allowOutsideClick:true,
			            confirmButtonColor: '#DD6B55',
			            confirmButtonText: 'Yes',
			            cancelButtonText: 'No',
			            closeOnConfirm: false
			        }, function(){
			            location.href = '".CRUDBooster::mainpath("terjual/")."'+id;
			        });
			    };


			        function perjalanan(id){
			        swal({
			            title: 'Do you want to perjalanan ?',
			            type:'info',
			            showCancelButton:true,
			            allowOutsideClick:true,
			            confirmButtonColor: '#DD6B55',
			            confirmButtonText: 'Yes',
			            cancelButtonText: 'No',
			            closeOnConfirm: false
			        }, function(){
			            location.href = '".CRUDBooster::mainpath("perjalanan/")."'+id;
			        });
			    };


			    	$('#atc').on('click',function() {
			    		var dataId =$(this).data('id');
			    		console.log(dataId);
			    		document.getElementById('id_atc').value=dataId;
			    		});

			    		function modalclose1(){
			    			$('#approve').modal('toggle');
			    		}; 
";


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = '
                              <div class="modal fade" id="dijual" role="dialog">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Harga Jual Ikan</h4>
                                    </div>
                                    <div class="modal-body">
                                    <form method="post" action="'.CRUDBooster::mainpath("dijual").'" enctype="multipart/form-data">
                                    '.csrf_field().'
                                    <input type="hidden" id="id_atc" name="id">
                                    <br>
                                    <br>
                                    <div class="form-group">
                                    <label class="col-sm-3" for="exampleInputFile">Harga</label>
                                    <div class="col-sm-9">
                                    <input class="form-control" name="harga" multiple type="number" id="exampleInputFile">
                                    </div>
                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="submit" id="submit1" onclick="modalclose1()" class="btn btn-primary">Save</button>
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                   ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here

	        // $query->where('status','Approve')->orWhere('status','waiting');
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }


	    public function getApprove($id){
	    	$ubah['status']="Approve";

	    	$cek=DB::table('data_ikan')->where('id',$id)->update($ubah);
	    	if ($cek) {				
	    		$res = redirect()->back()->with(["message"=>"Succesfully change status",'message_type'=>'success'])->withInput();
	    	}else{
	    		$res = redirect()->back()->with(["message"=>"Error change status",'message_type'=>'warning'])->withInput();
	    	}
	    	\Session::driver()->save();
	    	$res->send();
	    	exit();
	    }

	      public function getDijual($id){
	    	$ubah['status']="Dijual";

	    	$cek=DB::table('data_ikan')->where('id',$id)->update($ubah);
	    	if ($cek) {				
	    		$res = redirect()->back()->with(["message"=>"Succesfully change status",'message_type'=>'success'])->withInput();
	    	}else{
	    		$res = redirect()->back()->with(["message"=>"Error change status",'message_type'=>'warning'])->withInput();
	    	}
	    	\Session::driver()->save();
	    	$res->send();
	    	exit();
	    }

	    public function postDijual(){
	    	$ubah['harga']=g('harga');
	    	$ubah['status']="Dijual";
	    	$id=g('id');
	    	$cek=DB::table('data_ikan')->where('id',$id)->update($ubah);
	    		if ($cek) {				
	    		$res = redirect()->back()->with(["message"=>"Succesfully change status",'message_type'=>'success'])->withInput();
	    	}else{
	    		$res = redirect()->back()->with(["message"=>"Error change status",'message_type'=>'warning'])->withInput();
	    	}
	    	\Session::driver()->save();
	    	$res->send();
	    	exit();
	    }

	    //   public function getTerjual($id){
	    // 	$ubah['status']="Terjual";

	    // 	$cek=DB::table('data_ikan')->where('id',$id)->update($ubah);
	    // 	if ($cek) {				
	    // 		$res = redirect()->back()->with(["message"=>"Succesfully change status",'message_type'=>'success'])->withInput();
	    // 	}else{
	    // 		$res = redirect()->back()->with(["message"=>"Error change status",'message_type'=>'warning'])->withInput();
	    // 	}
	    // 	\Session::driver()->save();
	    // 	$res->send();
	    // 	exit();
	    // }



	    //By the way, you can still create your own method in here... :) 


	}