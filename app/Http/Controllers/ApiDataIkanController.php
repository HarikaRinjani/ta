<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiDataIkanController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "data_ikan";        
				$this->permalink   = "data_ikan";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
		        $date=date('Y-m');
		        $id_data_ikan    =g('id_jenis_ikan');
		        $nama            =g('nama');
		        $size            =g('size');
		        $catatan         =g('catatan');
		        $created_at      =date('Y-m-d H:i:s');
		        $image_1		 = Request::file('image_1');
		    	$image_2		 = Request::file('image_2');
		    	$image_3		 = Request::file('image_3');
		    	$image_4		 = Request::file('image_4');

		    	$save['id_jenis_ikan']=$id_data_ikan;
		    	$save['nama']=g('nama');
		    	$save['size']=g('size');
		    	$save['catatan']=g('catatan');
		    	$save['created_at']=$created_at;

		    	$eks		= DB::table('data_ikan')->insertGetId($save);

		    	//folder
		    	if($eks){
		    	$folder = 'uploads/1/'.$date;
		    	if ($image_1!='') {
			    		$img_1     = rand(11111,99999).".png";
		                $image_1->move($folder,$img_1);
		                $image_1   = $folder.'/'.$img_1;
			    	}

			    	if ($image_2!='') {
			    		$img_2     = rand(11111,99999).".png";
		                $image_2->move($folder,$img_2);
		                $image_2   = $folder.'/'.$img_2;
			    	}

			    	if ($image_3!='') {
			    		$img_3     = rand(11111,99999).".png";
		                $image_3->move($folder,$img_3);
		                $image_3   = $folder.'/'.$img_3;
			    	}

			    	if ($image_4!=''){
			    		$img_4     = rand(11111,99999).".png";
		                $image_4->move($folder,$img_4);
		                $image_4   = $folder.'/'.$img_4;
			    	}

			    	$surv_foto=[];

			    	$list['id_data_ikan']=$eks;
			    	$list['image']=url($image_1);

			    	$list1['id_data_ikan']=$eks;
			    	$list1['image']=url($image_2);

			    	$list2['id_data_ikan']=$eks;
			    	$list2['image']=url($image_3);

			    	$list3['id_data_ikan']=$eks;
			    	$list3['image']=url($image_4);

			    	array_push($surv_foto, $list);
			    	array_push($surv_foto, $list1);
			    	array_push($surv_foto, $list2);
			    	array_push($surv_foto, $list3);

			    	$fish=DB::table('image_koi')->insert($surv_foto);
			    }

			    if($fish){
			    	$response['api_status']  	   = 1;
			    	$response['api_message'] 	   = 'success';
			    	$response['api_authorization'] = 'You are in debug mode !';
			    	$response['api_http']		   = 200;
			    	response()->json($response)->send();
			    	exit();
			    }else{
			    	$response['api_status']  	   = 0;
			    	$response['api_message'] 	   = 'Faild';
			    	$response['api_authorization'] = 'You are in debug mode !';
			    	$response['api_http']		   = 200;
			    	response()->json($response)->send();
			    	exit();

			    }


		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}