<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use CRUDBooster;
use Session;
use Response;

class LoginController extends Controller
{
    public function register(){
    	return view('register');
    }

      public function login(){
    	return view('login');
    }

    public function register_member(Request $request){
        if($request->id_terms !=null){
    	$nama=$request->nama;
        $id_terms=$request->id_terms;
    	$password=$request->password;
    	$retype=$request->retype;
    	$hp=$request->hp;
    	$email=$request->email;
    	if($password == $retype){
            $data['id_terms']=$id_terms;
    		$data['nama']=$nama;
    		$data['password']=Hash::make($password);
    		$data['hp']=$hp;
    		$data['email']=$email;
    		$kw=DB::table('users')->insert($data);
    		if($kw){
    			 return redirect()->back()->with(['message'=>'Register Berhasil','message_type'=>'primary']);
    		}else{
    			return redirect()->back()->with(['message'=>'Register Gagal','message_type'=>'warning']);
    		}
    	}else{
    		return redirect()->back()->with(['message'=>'Isikan Password Dengan Benar','message_type'=>'warning']);
    	}

    }else{
    return redirect()->back()->with(['message'=>'Silahkan Setujui Ketentuan Website Ini','message_type'=>'danger']);
}
}

    public function login_member(Request $request){
        if($request->id_terms !=null){

        $email=$request->email;
        $password=$request->password;
        if($email==''){
            return redirect()->back()->with(['message'=>'Harap isi email dengan benar','message_type'=>'warning']);
        }elseif($request->password==''){
            return redirect()->back()->with(['message'=>'Harap isi password','message_type'=>'warning']);
        }
        elseif($email !=''){
            $cek=DB::table('users')->where('email',$email)->first();
            if(!(empty($cek))){
                if(!Hash::check($password, $cek->password)){
                    return redirect()->back()->with(['message'=>'password salah','message_type'=>'warning']);
                }else{
                    Session::put('id',$cek->id);
                    Session::put('nama',$cek->nama);
                    return redirect()->action('FrontController@index');
                }
            }else{
                return redirect()->back()->with(['message'=>'Email belum terdaftar','message_type'=>'warning']);
            }
        }else{
            return redirect()->back()->with(['message'=>'Harap login dengan data yang benar','message_type'=>'warning']);
        }
    }else{

        return redirect()->back()->with(['message'=>'Silahkan Setujui Ketentuan Di Website Ini','message_type'=>'danger']);

    }
    	
    }


    public function logout(){
        session()->flush();
        return redirect('login')->with(['message'=>'Terimakasih','message_type'=>'success']);
    }
}
